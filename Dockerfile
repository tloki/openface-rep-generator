FROM ubuntu:18.04

WORKDIR /root

RUN apt update
RUN apt install python3-dev python3-pip python3-setuptools git cmake software-properties-common -y

COPY /requirements.txt /root
COPY /trainedModels /root/trainedModels
RUN pip3 install

RUN git clone https://github.com/cmusatyalab/openface.git
RUN git clone https://github.com/torch/distro.git ~/torch --recursive

ENV LUA_PATH='/root/.luarocks/share/lua/5.1/?.lua;/root/.luarocks/share/lua/5.1/?/init.lua;/root/torch/install/share/lua/5.1/?.lua;/root/torch/install/share/lua/5.1/?/init.lua;./?.lua;/root/torch/install/share/luajit-2.1.0-alpha/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua' \
    LUA_CPATH='/root/.luarocks/lib/lua/5.1/?.so;/root/torch/install/lib/lua/5.1/?.so;./?.so;/usr/local/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/loadall.so' \
    PATH=/root/torch/install/bin:$PATH \
    LD_LIBRARY_PATH=/root/torch/install/lib:$LD_LIBRARY_PATH \
    DYLD_LIBRARY_PATH=/root/torch/install/lib:$DYLD_LIBRARY_PATH


COPY /requirements.txt /root

RUN ls -alh

RUN pip3 install -r requirements.txt

RUN apt install sudo -y

# hack for 18.04
RUN sed -i -e 's/python-software-properties/software-properties-common/g' torch/install-deps

RUN cd torch && bash install-deps
RUN cd torch && ./install.sh

RUN cd openface && python3 setup.py install

RUN luarocks install dpnn
RUN luarocks install nn
RUN luarocks install optim
RUN luarocks install csvigo

COPY / /root

EXPOSE 80

ENTRYPOINT [ "python3", "-u", "main.py" ]