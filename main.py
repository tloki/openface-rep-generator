from configuration import *
import cv2
import openface
import time
from FaceRecognizer import *
import json
import os
import io
from flask import Flask, request
from PIL import Image
from scipy import spatial


haar_face_detector = cv2.CascadeClassifier(FACE_CASCADE_PATH)
align = openface.AlignDlib(DLIB_FACE_PREDICTOR_MODEL_PATH)
net = openface.TorchNeuralNet(FACE_EMBEDDING_MODEL_PATH, IMG_DIM_PARAM)


app = Flask(__name__)
# 15MB Max image size limit
app.config['MAX_CONTENT_LENGTH'] = 15 * 1024 * 1024


# Default route just shows simple text
@app.route('/')
def index():
    return 'it works!'


def pil_to_img(image):
    w, h = image.size
    # print('Image size', w, 'x', h)

    augmented_image = np.asarray(image)

    return augmented_image


REP = None
RGB = None


@app.route('/get_rep', methods=['POST'])
def predict_image_handler():
    try:
        image_data = None
        if 'imageData' in request.files:
            print("imageData postoji")
            image_data = request.files['imageData']
        else:
            print("mora postojat imageData, probaj s 'curl -F \"imageData=@/home/loki/proj/openface-rep-generator/sample_image.jpg\" localhost/get_rep'")
            raise Exception

        # print("otvaram sliku")
        img_raw = Image.open(image_data)
        #
        # print("pretvaram sliku u np array:")
        img = pil_to_img(img_raw)
        # img = img[..., ::-1]
        #
        #
        # print("dimenzije slike")
        # print(img.shape)

        # print("tip slike")
        # print(type(img))
        #
        # print("jesu li slike iste:")
        # print((RGB == img).any())
        #
        # print("ugradena slika:")
        # print(RGB)
        # print("slika primljena preko mreze")
        # print(img)

        results = list(image_to_rep(img))
        # print(results.shape)
        # print(results == REP)
        #
        # distance = spatial.distance.cosine(results, REP)
        # print("distance:", distance)
        return json.dumps(results)
    except Exception as e:
        print('EXCEPTION:', str(e))
        return 'Error processing image\n', 500


def image_to_rep(rgb):
    bb = find_face_fast(rgb, haar_face_detector)
    if len(bb) != 1:
        return False
    bb = bb[0]
    print(bb)
    aligned_face = align_face(rgb, align, IMG_DIM_PARAM, bb)
    rep = generate_rep_vector(aligned_face, net)
    return rep


def run_sample_image():
    global REP
    global RGB
    rgb = get_rgb_image_from_file("sample_image.jpg")
    rep = image_to_rep(rgb)
    REP = rep
    RGB = rgb
    print(rep)
    print(rgb.shape)


if __name__ == "__main__":
    print("proba:")
    run_sample_image()
    print("proba ok")
    # Run the server
    app.run(host='0.0.0.0', port=80)

# while True:
#     run_sample_image()
#     time.sleep(10)