DISPLAY = False
VERBOSE = True

FACE_CASCADE_PATH = "trainedModels/face.xml"

CAMERA_CAPTURE_WIDTH = 320
CAMERA_CAPTURE_HEIGHT = 240
CAMERA_MAX_FRAME_RATE = 120
# not to be confused with scaled-detection photo!


# raspicam hack:
# sudo modprobe bcm2835-v4l2

# Parameters for haar detection
# From the API:
# The default parameters (scale_factor=2, min_neighbors=3, flags=0) are tuned
# for accurate yet slow object detection. For a faster operation on real video
# images the settings are:
# scale_factor=1.2, min_neighbors=2, flags=CV_HAAR_DO_CANNY_PRUNING,
# min_size=<minimum possible face size
MIN_SIZE = (5, 5)
HAAR_SCALE = 1.2
MIN_NEIGHBOURS = 3
HAAR_FLAGS = 0

# detection image width i
# 70 fast frame rate
# 90 - more accurate / smaller faces / further away
# 120+ slow
SMALL_WIDTH = 200
PAN = 100

IS_RPI = False

DLIB_FACE_PREDICTOR_MODEL_PATH = "trainedModels/shape_predictor_68_face_landmarks.dat"
FACE_EMBEDDING_MODEL_PATH = "trainedModels/nn4.small2.v1.amd64.t7"
IMG_DIM_PARAM = 96
NP_PRINT_PRECISION = 2